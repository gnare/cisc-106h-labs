from cisc106 import assertEqual

animal_species = ["Dog", "House Cat", "Lion", "Red Fox", "Hard Clam", "Eastern Bluebird",
                  "Great White Shark", "Common Box Turtle", "Woodland Hedgehog", "Common Wombat"]
animal_weights = [50, 15, 260, 30, 1, 1, 2400, 2, 2, 77]


def make_pairs(klist, vlist):
    """
    Creates a dictionary of key (klist)-value (vlist) pairs for two lists of equal length.
    Uses a for loop to pair elements.
    """
    pair_dict = {}
    len_keys = len(klist)
    len_values = len(vlist)
    for i in range(min(len_keys, len_values)):
        pair_dict[klist[i]] = vlist[i]
    return pair_dict


assertEqual(make_pairs([], []), {})
assertEqual(make_pairs(['key1'], [6]), {'key1': 6})
assertEqual(make_pairs(['echidna', 'hedgehog', 'wombat'], [True, False, 7]), {'echidna': True, 'hedgehog': False, 'wombat': 7})
assertEqual(make_pairs(animal_species, animal_weights), {'Dog': 50, 'House Cat': 15, 'Lion': 260, 'Red Fox': 30, 'Hard Clam': 1, 'Eastern Bluebird': 1, 'Great White Shark': 2400, 'Common Box Turtle': 2, 'Woodland Hedgehog': 2, 'Common Wombat': 77})


def make_pairs2(klist, vlist):
    """
        Creates a dictionary of key (klist)-value (vlist) pairs for two lists of equal length.
        Uses the zip() function to pair elements.
    """
    return dict(zip(klist, vlist))


assertEqual(make_pairs2([], []), {})
assertEqual(make_pairs2(['key1'], [6]), {'key1': 6})
assertEqual(make_pairs2(['echidna', 'hedgehog', 'wombat'], [True, False, 7]), {'echidna': True, 'hedgehog': False, 'wombat': 7})
assertEqual(make_pairs2(animal_species, animal_weights), {'Dog': 50, 'House Cat': 15, 'Lion': 260, 'Red Fox': 30, 'Hard Clam': 1, 'Eastern Bluebird': 1, 'Great White Shark': 2400, 'Common Box Turtle': 2, 'Woodland Hedgehog': 2, 'Common Wombat': 77})
