# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def sumNoDups(a, b, c):
    numbers = [a, b, c]  # make the parameters iterable
    for n in numbers:  # iterate the numbers
        if numbers.count(n) > 1:  # check if there are duplicates in the list
            numbers = list(filter(lambda z: z != n, numbers))  # eliminate duplicates
    return sum(numbers)  # sum and return the remaining numbers


assertEqual(sumNoDups(6, 6, 6), 0)  # test with all duplicates
assertEqual(sumNoDups(200, 200, 42), 42)  # test with 2 duplicates
assertEqual(sumNoDups(1, 2, 3), 6)  # test with no duplicates
