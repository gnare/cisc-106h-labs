def merge(asorted, bsorted):
    if not asorted:
        return bsorted
    elif not bsorted:
        return asorted
    else:
        if asorted[0] < bsorted[0]:
            return [asorted[0]] + merge(asorted[1:], bsorted)
        else:
            return [bsorted[0]] + merge(asorted, bsorted[1:])

def merge_sort(alist):
    if len(alist) <= 1:
        return alist
    else:
        mid = len(alist) // 2
        return merge(merge_sort(alist[:mid]), merge_sort(alist[mid:]))
