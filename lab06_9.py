from cisc106 import assertEqual


def count_singles(key, alist):
    """
    Counts the number of occurrences of key in alist. Does not count an occurrence if it is adjacent to itself in the
    list.
    """
    count = 0
    last_two = [None, None]  # this avoids index out of bounds errors and achieves the same thing as alist[i-1] or [i-2]
    for elt in alist:
        if key == elt:
            count += 1
            if last_two[0] == elt:
                count -= 2
                if last_two[1] == elt:
                    count += 1  # fixes a case where odd numbers of consecutive occurrences cause it to subtract 1 extra
        last_two[1] = last_two[0]  # remember the last two items in the list (0 is most recent)
        last_two[0] = elt
    return count


assertEqual(count_singles(7, []), 0)
assertEqual(count_singles(1, [1, 1, 1]), 0)
assertEqual(count_singles(2, [700, 2, 2, 4, 2, 2]), 0)
assertEqual(count_singles(9, [9, 7, 9, 9, 0, 5, 9]), 2)
assertEqual(count_singles(2, [2, 3, 2, 2, 5, 2, 7, 2, 2, 2, 8, 9, 2]), 3)
