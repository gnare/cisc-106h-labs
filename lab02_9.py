from cisc106 import assertEqual


def concatenate(par1_list, par2_list):
    """
    Adds two lists together and returns a single list
    """
    if not par1_list:
        return par2_list
    else:
        return [par1_list[0]] + concatenate(par1_list[1:], par2_list)


assertEqual(concatenate([], []), [])
assertEqual(concatenate(['a', 'b'], ['c', 'd']), ['a', 'b', 'c', 'd'])
assertEqual(concatenate([], [1, 2, 3]), [1, 2, 3])
