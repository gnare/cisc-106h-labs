import tkinter as tk


class MyGame:
    def __init__(self, master):
        """This is the constructor MyGame() that takes a single tk window as
        parameter. (We do not pass in a parameter for self, Python
        does that.) It builds a MyGame object that has a title, two
        button widgets, and a canvas widget. Identify those four
        pieces in the code of the method and show your partners before you proceed.

        """
        self.master = master
        self.square = None
        self.mouse_x = 0
        self.x = 0
        self.mouse_y = 0
        self.y = 0
        master.title("Game")

        self.square_button = tk.Button(master, text="Square", command=self.draw_square)
        self.square_button.pack()  # pack() places the button on the Canvas

        self.close_button = tk.Button(master, text="Close", command=master.destroy)
        self.close_button.pack()

        canvas_width = 600
        canvas_height = 400
        self.canvas = tk.Canvas(master, width=canvas_width, height=canvas_height)
        self.canvas.create_line(0, 0, canvas_width, canvas_height)
        self.canvas.create_line(0, canvas_height, canvas_width, 0)
        self.canvas.pack()

    def move_rectangles(self):
        print(self.mouse_x, self.mouse_y)
        if not self.square:
            root_window.after(16, self.move_rectangles)
        else:
            if self.mouse_x < self.x:
                self.x -= 1
                self.canvas.move(self.square, -1, 0)
            else:
                self.x += 1
                self.canvas.move(self.square, 1, 0)
            if self.mouse_y < self.y:
                self.y -= 1
                self.canvas.move(self.square, 0, -1)
            else:
                self.y += 1
                self.canvas.move(self.square, 0, 1)
            root_window.after(16, self.move_rectangles)

    def mouse_click(self, event):
        self.mouse_x = event.x
        self.mouse_y = event.y

    def draw_square(self, x=50, y=50):
        self.x = 50
        self.y = 50
        self.square = self.canvas.create_rectangle(x, y, x + 50, y + 50, fill="blue")


root_window = tk.Tk()
my_gui = MyGame(root_window)
root_window.bind("<1>", my_gui.mouse_click)
root_window.after(16, my_gui.move_rectangles)
root_window.mainloop()
