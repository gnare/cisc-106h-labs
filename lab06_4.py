from cisc106 import assertEqual


def dec_to_binary(n):
    """
    Returns the binary representation of a number. This function does not work for negative numbers.
    """
    target_pow = 0
    binary_str = ''
    while 2 ** target_pow <= n and not 2 ** (target_pow + 1) > n:
        target_pow += 1
    while target_pow >= 0:
        temp = 2 ** target_pow
        if n >= temp:
            n -= temp
            binary_str += '1'
        else:
            binary_str += '0'
        target_pow -= 1
    return binary_str


assertEqual(dec_to_binary(347), '101011011')
assertEqual(dec_to_binary(0), '0')
assertEqual(dec_to_binary(1), '1')
assertEqual(dec_to_binary(65535), '1111111111111111')
