from cisc106 import assertEqual


def reverse(par1_list):
    """
        Reverses the list
    """
    if not par1_list:
        return []  # end recursion if end of list is reached
    else:
        return reverse(par1_list[1:]) + [par1_list[0]]  # take last element in list and put it in the front


assertEqual(reverse([]), [])
assertEqual(reverse([1, 2, 3, 4]), [4, 3, 2, 1])
assertEqual(reverse([11, 42, 56, 324]), [324, 56, 42, 11])
