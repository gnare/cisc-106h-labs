from cisc106 import assertEqual


def my_nth(n, par2_list):
    """
        Gets the element at position n in the list.
    """
    if not n:
        return par2_list[0]  # element found
    else:
        return my_nth(n - 1, par2_list[1:])  # advance in the list by one


assertEqual(my_nth(2, [0, 1, 2, 3]), 2)
assertEqual(my_nth(0, [10, 4]), 10)
assertEqual(my_nth(1, [4, -1, 4, 3]), -1)
