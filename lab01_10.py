# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def positive(n):
    if n < 0:
        return n * -1
    else:
        return n


assertEqual(positive(4), 4)
assertEqual(positive(-7), 7)
assertEqual(positive(0), 0)
