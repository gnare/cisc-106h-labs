import random
import timeit

from matplotlib import pyplot

from cisc106 import assertEqual


def mapped_add(a_elt, blist):
    result = []
    for elt in blist:
        result += [elt + [a_elt]]
    return result


assertEqual(mapped_add(7, []), [])
assertEqual(mapped_add(8, [[42]]), [[42, 8]])
assertEqual(mapped_add('e', [['a'], ['b'], ['c']]), [['a', 'e'], ['b', 'e'], ['c', 'e']])


def allcombinations(alist):
    """
    Returns all possible combinations of alist, with no duplicates.
    """
    if not alist:  # base cases to prevent unnecessary computation
        return alist
    elif len(alist) == 1:
        return [alist]
    else:
        result = []
        for elt in alist:
            result += mapped_add(elt, result) + [[elt]]
        return result


assertEqual(allcombinations([]), [])
assertEqual(allcombinations([5]), [[5]])
assertEqual(allcombinations([1, 2, 3]), [[1], [1, 2], [2], [1, 3], [1, 2, 3], [2, 3], [3]])

alist = list(range(1, 21))
random.shuffle(alist)

dependent = []
range_obj = range(1, 21)
for list_length in range_obj:
    short_list = alist[:list_length]
    elapsed = timeit.timeit("allcombinations(short_list)", number=1, globals=globals())
    dependent += [elapsed]
pyplot.plot(range_obj, dependent, 'r', label="allcombinations")

pyplot.title('Allcombinations Timings (n=1)')
pyplot.xlabel('List Size')
pyplot.ylabel('Time (seconds)')
pyplot.legend()
pyplot.savefig('lab07_13.png')
pyplot.show()
