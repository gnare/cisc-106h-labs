def multTable(n):
    """
    Prints a multiplication table with factors up to n.
    """
    table_range = range(n + 1)
    for i in table_range:
        strbuf = ''
        for j in table_range:
            nSpaces = len(str(n ** 2)) - len(str(i * j)) + 1
            strbuf += ' ' * nSpaces + str(i * j)
        print(strbuf)
