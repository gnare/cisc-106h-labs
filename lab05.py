import matplotlib.pyplot as plt

plt.figure(1)
nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
plt.plot(nums, nums)


def square(x):
    return x * x


dependent = list(map(square, nums))
plt.plot(nums, dependent)
plt.title('Parabola and Data')
plt.xlabel('x')
plt.ylabel('y')
plt.savefig('lab05.png')
plt.show()
