from cisc106 import assertEqual


def is_even(n):
    """
    A predicate that returns true if n is an even number.
    """
    return n % 2 == 0


def filter(predicate, par2_list):
    """
    Filters out elements in a list based on a predicate helper function.
    The predicate must take exactly 1 paramter.
    """
    if not par2_list:
        return []
    else:
        if predicate(par2_list[0]):
            return [par2_list[0]] + filter(predicate, par2_list[1:])
        else:
            return filter(predicate, par2_list[1:])


assertEqual(filter(is_even, []), [])
assertEqual(filter(is_even, [-2, 0, 2, 3]), [-2, 0, 2])
assertEqual(filter(is_even, [11, 24, 32, 64, 127, 255, 512]), [24, 32, 64, 512])
