function newPic = drawBigX(pic, layer, newValue, thickness)  
  newPic = pic; 
  for col = 1:columns(pic)
    for row = 1:rows(pic)
      if isWithinTolerance(col, row, thickness - 1) | isWithinTolerance(rows(pic) - col + 1, row, thickness - 1)
        newPic(row, col, layer) = newValue;
      end
    end
  end
end