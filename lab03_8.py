from cisc106 import assertEqual


def min(a, b):
    """
    Returns a or b, whichever is lower.
    """
    if a < b:
        return a
    else:
        return b


def accumulate(fcn, par2_list, init):
    """
    Compares the items in the list and returns the result based on the function parameter.
    The init parameter should be the first element in the list.
    """
    if not par2_list:
        return init
    else:
        return fcn(par2_list[0], accumulate(fcn, par2_list[1:], init))


assertEqual(accumulate(min, [], 7), 7)
assertEqual(accumulate(min, [1, 2, 3, 4], 1), 1)
assertEqual(accumulate(min, [3.14, -4, 65, 42], 3.14), -4)
