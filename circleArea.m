function result = circleArea(radius)
  result = pi * radius ^ 2; 