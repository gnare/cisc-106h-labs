from cisc106 import assertEqual


def nested_reverse(par1_list):
    """
    Reverses a list that can contain other lists.
    Lists that are nested will also be reversed.
    This function does not care about the types of list elements.
    """
    if not par1_list:
        return []  # end of list. stop.
    if isinstance(par1_list[0], list):
        return nested_reverse(par1_list[1:]) + [
            nested_reverse(par1_list[0])]  # next item is a list, reverse it and add it to the end
    else:
        return nested_reverse(par1_list[1:]) + [par1_list[0]]  # next item is not a list, add it to the end


assertEqual(nested_reverse([1, 3, [4, [], [2, [1]]]]), [[[[1], 2], [], 4], 3, 1])
assertEqual(nested_reverse([1, 2, [3, 4], 5, 6]), [6, 5, [4, 3], 2, 1])
assertEqual(nested_reverse([]), [])
