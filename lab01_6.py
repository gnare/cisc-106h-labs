# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def rectangleArea(l, w):
    return l * w  # calculates and returns the area of a rectangle


assertEqual(rectangleArea(3, 4), 12)  # test that the function correctly calculates area
assertEqual(rectangleArea(5, 5), 25)
assertEqual(rectangleArea(10, 20), 200)
