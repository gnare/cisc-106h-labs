"""Galen Nare, Derik Spedden, & Will Quintana"""
from cisc106 import *
import time
from tkinter import *

DEBUG = True


# A grid is a 2D list containing ones or zeros. A "shape" is an
# object containing a nested tuple, where the real world shape is
# represented as True values. For example, an "L" shape could be
# represented as
#     ((False, False, True),(True, True, True))
# Read a sequence of letters from a string (later from a text
# file). Each letter represents a shape: T, L, or Z (all 2x3), or I
# (1x4). Use functions  to find a place on the grid where
# the next shape in the sequence doesn't overlap any ones. Rotate the
# shape to find the "best" fit. Then modify the grid to show the
# shape in place, and repeat with the next shape in the sequence.
# We will add the graphics and high level functions in the next part.
class Grid:
    """Has attributes numRows, numCols, and squares (a 2D list containing True/False)."""

    def __init__(self, rows, cols, squares):
        self.numRows = rows
        self.numCols = cols
        self.squares = squares if squares else [cols * [False] for r in range(rows)]

    def update_grid(self, shape):
        """Top-level def for placing a single shape on grid. Finds best
        position and rotation for shape, update grid squares (mutates).
        Calls: find_max_score_location
        Returns boolean: fits or not"""
        shape_fits, n_rotations, loc_tuple = find_max_score_location(self, shape)
        shape.y, shape.x = loc_tuple
        for i in range(n_rotations):
            shape.rotate90()
        if shape_fits:
            for row in range(len(shape.squares)):
                for col in range(len(shape.squares[row])):
                    if shape.squares[row][col]:
                        self.squares[loc_tuple[0] + row][loc_tuple[1] + col] = True
        return shape_fits

    def print(self):
        """Print the grid, using an asterisk for each true square, underscore for false.
        Use a newline after each row, and no spaces in rows."""
        grid_print = ''
        row_len = len(self.squares)
        col_len = len(self.squares[0])

        for row in range(row_len):
            grid_print += '\n'
            for column in range(col_len):
                if self.squares[row][column]:
                    grid_print += '*'
                else:
                    grid_print += '_'
        print(grid_print)


class Shape:
    """A "shape" is a nested tuple, where the real world shape is
    represented as ones. For example, an "L" shape could be
    represented as((False, False, True),(True, True, True))
    Has attributes x, y, letter, squares (a nested list of type boolean),color, num_rotations"""

    def __init__(self, letter, squares, color):
        self.x = 0  # will be modified later to indicate position
        self.y = 0  # will be modified later to indicate position2
        self.letter = letter
        self.squares = squares
        self.color = color
        self.num_rotations = 0

    def rotate90(self):
        """Rotates this shape 90 degrees clockwise (direction matters).
        Mutates squares and num_rotations
        Returns None"""
        if not self.squares:
            return self.squares
        self.squares = [list(new_col) for new_col in zip(*self.squares[::-1])]
        self.num_rotations += 1
        if self.num_rotations == 4:
            self.num_rotations = 0
        return self.squares


def generate_all_locations(grid, shape):
    """Takes a single shape in one position and finds all the places it could fit on the
    grid, given its dimensions.
    Returns: a list of row,col tuples"""
    shape_height = len(shape.squares)
    shape_width = len(shape.squares[0])
    result = []
    if shape_height > len(grid.squares) or shape_width > len(grid.squares[0]):
        return result
    for row in range(len(grid.squares)):
        for column in range(len(grid.squares[row])):
            if len(grid.squares) >= (row + shape_height) and len(grid.squares[row]) >= (column + shape_width):
                result += [(row, column)]
            else:
                pass
    return result


def get_valid_locations(location_list, grid, shape):
    """Returns list of locations where shape does not overlap shapes
    already on grid. Assumes that all locations in parameter list are
    potential fits for this shape.
    Calls: fits"""
    valid_locations = []
    for coord in location_list:
        if fits(coord, grid, shape):
            valid_locations += [tuple(coord)]
    return valid_locations


def fits(location, grid, shape):
    """Returns True if shape placed at location does not overlap shapes
    already on grid.location: row,col tuple"""
    for row in range(location[0], location[0] + (len(shape.squares))):
        if row < (location[0] + len(shape.squares)) and row < len(grid.squares):
            for column in range(location[1], (location[1] + len(shape.squares[row - location[0]]))):
                if column < (location[1] + len(shape.squares[row - location[0]])) and column < len(grid.squares[row]):
                    if grid.squares[row][column] == shape.squares[row - location[0]][column - location[1]] and shape.squares[row - location[0]][column - location[1]]:
                        return False
                else:
                    return False
        else:
            return False
    return True


def get_max_score(location_list, grid, shape):
    """Finds highest scoring location from list, given shape.When scores are equal,
    the lowest row (highest row number), right end (highest
    column) should be preferred.
    Return: nested tuple of (location_tuple,number)
    Calls: get_score"""
    current_max_score = 0
    best_location = (0, 0)
    for loc in location_list:
        loc_score = get_score(loc, grid, shape)
        if current_max_score < loc_score:
            current_max_score = loc_score
            best_location = loc
        elif current_max_score == loc_score:
            if loc[1] > best_location[1]:
                best_location = loc
            elif current_max_score == loc_score:
                if loc[0] > best_location[0]:
                    best_location = loc
    return best_location, current_max_score


def get_score(location, grid, shape):
    """Computes the score for a shape placed at a single location on grid.
    Scores are positive, higher is better. For now, code the heuristic discussed in class.
    location: row,col tuple
    Returns: number"""
    score = 0
    for row in range(location[0], location[0] + (len(shape.squares))):
        if row < (location[0] + len(shape.squares)) and row < len(grid.squares):
            for column in range(location[1], (location[1] + len(shape.squares[row - location[0]]))):
                if column < (location[1] + len(shape.squares[row - location[0]])) and column < len(grid.squares[row]):
                    if grid.squares[row][column] and (not shape.squares[row - location[0]][column - location[1]]):
                        score += 1
    score += bonus(location, grid, shape)
    return score

    
def bonus(location, grid, shape):
    """Returns a score bonus for placing a shape surrounded by a True"""
    bonus_score = 0
    for check in range(2):
        if check == 0:
            if (location[0] - 1) >= 0:
                for top in range(location[1], len(shape.squares[0])):
                    if grid.squares[location[0] - 1][top] == True:
                        bonus_score += .01
            else:
                bonus_score += .02 * len(shape.squares[0])
        if check == 1:
            if (location[0] + len(shape.squares) + 1) < len(grid.squares):
                for bottom in range(location[1], len(shape.squares[0]) + 1):
                    if grid.squares[location[0] + len(shape.squares)][bottom] == True:
                        bonus_score += .02
            else:
                bonus_score += .02 * len(shape.squares[0])
    return bonus_score

def find_max_score_location(grid, shape):
    """Takes a single shape, finds best position on grid. Tries original
    and three possible 90 degree rotations. Mutates shape for each rotation.
    When scores are equal, the lowest row (highest row number), right end (highest
    column) should be preferred.
    Returns tuple:  (fits numberRotations location)
    fits: bool
    maxScoreRow, maxScoreCol: upper left coordinates of best position for shape on grid
    numberRotations: 0-3 rotations required for best fit.
    Calls: rotate90, generate_all_locations, get_valid_locations, get_max_score"""
    location_found = False
    max_location = (0, 0)
    max_score = 0
    max_rotation = 0
    for rotation in range(4):
        valid_loc_list = get_valid_locations(generate_all_locations(grid, shape), grid, shape)
        if valid_loc_list:
            temp_location, temp_score = get_max_score(valid_loc_list, grid, shape)
            if not location_found or temp_score > max_score or (temp_score == max_score and temp_location[1] > max_location[1]) or (temp_score == max_score and temp_location[1] == max_location[1] and temp_location[0] > max_location[0]):
                location_found = True
                max_location = temp_location
                max_score = temp_score
                max_rotation = shape.num_rotations
        shape.squares = shape.rotate90()
    return location_found, max_rotation, max_location


def get_shape(letter):
    """Returns the Shape corresponding to the letter parameter: I for a line; T for a T;
    L for an L on its back, foot to right;
    Z for a Z. More may be added. """
    shape_L = Shape('L', ((False, False, True), (True, True, True)), 'red')
    shape_Z = Shape('Z', ((True, True, False), (False, True, True)), 'blue')
    shape_I = Shape('I', [[True, True, True, True]], 'yellow')
    shape_T = Shape('T', ((True, True, True), (False, True, False)), 'green')
    if 'L' == letter:
        return shape_L
    elif 'Z' == letter:
        return shape_Z
    elif 'I' == letter:
        return shape_I
    elif 'T' == letter:
        return shape_T


if DEBUG:
    # updateGrid
    assertEqual(Grid(2, 4, []).update_grid(get_shape('L')), True)
    assertEqual(Grid(3, 2, []).update_grid(Shape('', [[True, False], [True, False], [True, True]], 'blue')), True)
    assertEqual(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]).update_grid(get_shape('L')), True)
    assertEqual(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]).update_grid(get_shape('T')), True)
    assertEqual(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]).update_grid(get_shape('Z')), True)
    assertEqual(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]).update_grid(get_shape('I')), True)
    assertEqual(Grid(2, 3, [[1, 0, 0], [0, 0, 0]]).update_grid(get_shape('T')), True)
    assertEqual(Grid(2, 3, [[1, 0, 0], [0, 0, 0]]).update_grid(get_shape('L')), True)
    assertEqual(Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]).update_grid(Shape('', [[True, True, False], [True, True, False]], 'blue')), True)
    assertEqual(Grid(3, 4, [[0, 0, 0, 0], [0, 0, 1, 1]]).update_grid(get_shape('L')), True)

    # rotate90
    assertEqual(get_shape('L').rotate90(), [[True, False], [True, False], [True, True]])
    assertEqual(Shape('', [[True, False], [True, False], [True, True]], 'blue').rotate90(), [[True, True, True], [True, False, False]])
    assertEqual(Shape('', [[True, True, True], [True, False, False]], 'blue').rotate90(), [[True, True], [False, True], [False, True]])
    assertEqual(get_shape('Z').rotate90(), [[False, True], [True, True], [True, False]])
    assertEqual(get_shape('I').rotate90(), [[True], [True], [True], [True]])
    assertEqual(get_shape('T').rotate90(), [[False, True], [True, True], [False, True]])
    assertEqual(Shape('', [[False, True], [True, True], [False, True]], 'blue').rotate90(), [[False, True, False], [True, True, True]])
    assertEqual(Shape('', [[False, True, False], [True, True, True]], 'blue').rotate90(), [[True, False], [True, True], [True, False]])
    assertEqual(Shape('', [], 'blue').rotate90(), [])
    assertEqual(Shape('', [[1, 2], [3, 4]], 'blue').rotate90(), [[3, 1], [4, 2]])
    assertEqual(Shape('', [[2, -4, 3], [5, 8, 1]], 'blue').rotate90(), [[5, 2], [8, -4], [1, 3]])
    assertEqual(Shape('', [[1, 2, 3]], 'blue').rotate90(), [[1], [2], [3]])

    # generate_all_locations
    assertEqual(generate_all_locations(Grid(2, 4, []), get_shape('L')), [(0, 0), (0, 1)])
    assertEqual(generate_all_locations(Grid(1, 1, []), get_shape('L')), [])
    assertEqual(generate_all_locations(Grid(2, 3, []), get_shape('L')), [(0, 0)])
    assertEqual(generate_all_locations(Grid(3, 2, []), Shape('', [[True, False], [True, False], [True, True]], 'blue')), [(0, 0)])
    assertEqual(generate_all_locations(Grid(3, 2, []), get_shape('L')), [])
    assertEqual(generate_all_locations(Grid(3, 5, []), get_shape('L')), [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)])
    assertEqual(generate_all_locations(Grid(3, 5, []), get_shape('Z')), [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)])
    assertEqual(generate_all_locations(Grid(3, 5, []), Shape('', [[True], [True], [True], [True]], 'blue')), [])
    assertEqual(generate_all_locations(Grid(3, 5, []), get_shape('I')), [(0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)])
    assertEqual(generate_all_locations(Grid(2, 4, []), get_shape('T')), [(0, 0), (0, 1)])
    assertEqual(generate_all_locations(Grid(2, 2, []), Shape('', [[True, True, False], [True, True, False]], 'blue')), [])
    assertEqual(generate_all_locations(Grid(2, 4, []), Shape('', [[True, True, False], [True, True, False]], 'blue')), [(0, 0), (0, 1)])
    assertEqual(generate_all_locations(Grid(2, 2, []), Shape('', [[True, True], [True, True]], 'blue')), [(0, 0)])

    # get_valid_locations
    assertEqual(get_valid_locations([(0, 0), (0, 1)], Grid(2, 4, []), get_shape('L')), [(0, 0), (0, 1)])
    assertEqual(get_valid_locations([(0, 0)], Grid(2, 3, []), get_shape('L')), [(0, 0)])
    assertEqual(get_valid_locations([(0, 0)], Grid(3, 2, []), Shape('', [[True, False], [True, False], [True, True]], 'blue')), [(0, 0)])
    assertEqual(get_valid_locations([(0, 0), (0, 1)], Grid(2, 4, []), get_shape('T')), [(0, 0), (0, 1)])
    assertEqual(get_valid_locations([(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)], Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('L')), [(0, 1)])
    assertEqual(get_valid_locations([(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)], Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('Z')), [])
    assertEqual(get_valid_locations([(0, 0), (1, 0)], Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), [(1, 0)])
    assertEqual(get_valid_locations([(0, 0)], Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('L')), [(0, 0)])
    assertEqual(get_valid_locations([(0, 0), (0, 1)], Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('T')), [])
    assertEqual(get_valid_locations([(0, 0), (0, 1)], Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), [(0, 0)])

    # fits
    assertEqual(fits((0, 0), Grid(2, 4, []), get_shape('L')), True)
    assertEqual(fits((0, 1), Grid(2, 4, []), get_shape('L')), True)
    assertEqual(fits((0, 0), Grid(2, 3, []), get_shape('L')), True)
    assertEqual(fits((0, 0), Grid(3, 2, []), Shape('', [[True, False], [True, False], [True, True]], 'blue')), True)
    assertEqual(fits((0, 0), Grid(2, 4, []), get_shape('T')), True)
    assertEqual(fits((0, 1), Grid(2, 4, []), get_shape('T')), True)
    assertEqual(fits((0, 1), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('L')), True)
    assertEqual(fits((0, 0), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('L')), False)
    assertEqual(fits((0, 2), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('Z')), False)
    assertEqual(fits((2, 0), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), False)
    assertEqual(fits((1, 0), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), True)
    assertEqual(fits((0, 0), Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('L')), True)
    assertEqual(fits((0, 0), Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('T')), False)
    assertEqual(fits((0, 1), Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), False)
    assertEqual(fits((0, 0), Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), True)
    assertEqual(fits((0, 0), Grid(2, 2, []), get_shape('L')), False)
    assertEqual(fits((0, 0), Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('L')), True)

    # get_max_score
    assertEqual(get_max_score([(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)], Grid(3, 5, []), get_shape('L')), ((0, 2), 0.12))
    assertEqual(get_max_score([(0, 0), (0, 1)], Grid(2, 4, []), get_shape('L')), ((0, 1), 0.12))
    assertEqual(get_max_score([(0, 0)], Grid(2, 3, []), get_shape('L')), ((0, 0), 0.12))
    assertEqual(get_max_score([(0, 0), (0, 1)], Grid(2, 4, []), get_shape('T')), ((0, 1), 0.12))
    assertEqual(get_max_score([(0, 0)], Grid(2, 4, []), Shape('', [[True, False], [True, False], [True, True]], 'blue')), ((0, 0), 0.08))
    assertEqual(get_max_score([(0, 1)], Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('L')), ((0, 1), 1.12))
    assertEqual(get_max_score([(0, 0)], Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), ((0, 0), 0.08))
    assertEqual(get_max_score([(1, 0)], Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), ((1, 0), 0.1))
    assertEqual(get_max_score([(0, 0)], Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('L')), ((0, 0), 1.12))
    assertEqual(get_max_score([(0, 0), (0, 1)], Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('T')), ((0, 1), 0.12))
    assertEqual(get_max_score([(0, 0), (0, 1)], Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), ((0, 0), 2.12))
    assertEqual(get_max_score([(0, 0)], Grid(2, 3, [[0, 0, 1], [1, 0, 0]]), get_shape('Z')), ((0, 0), 2.12))
    assertEqual(get_max_score([(0, 0)], Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), Shape('', [[False, True, False], [True, True, True]], 'blue')), ((0, 0), 1.12))

    # get_score
    assertEqual(get_score((0, 0), Grid(2, 4, []), get_shape('L')), 0.12)
    assertEqual(get_score((0, 1), Grid(2, 4, []), get_shape('L')), 0.12)
    assertEqual(get_score((0, 1), Grid(2, 4, []), get_shape('T')), 0.12)
    assertEqual(get_score((0, 0), Grid(2, 4, []), Shape('', [[True, False], [True, False], [True, True]], 'blue')), 0.08)
    assertEqual(get_score((0, 1), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('L')), 1.12)
    assertEqual(get_score((0, 0), Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), 0.08)
    assertEqual(get_score((0, 0), Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('L')), 1.12)
    assertEqual(get_score((0, 0), Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), 2.12)
    assertEqual(get_score((0, 1), Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), 0.12)
    assertEqual(get_score((0, 0), Grid(2, 3, [[0, 0, 1], [1, 0, 0]]), get_shape('Z')), 2.12)

    # find_max_score_location
    assertEqual(find_max_score_location(Grid(2, 4, []), get_shape('L')), (True, 0, (0, 1)))
    assertEqual(find_max_score_location(Grid(3, 4, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 1, 1, 1]]), get_shape('L')), (True, 2, (1, 0)))
    g = Grid(2, 4, [[True, False, False, False], [False, False, False, False]])
    assertEqual(find_max_score_location(g, get_shape('L')), (True, 0, (0, 0)))
    assertEqual(find_max_score_location(g, get_shape('T')), (True, 2, (0, 0)))
    assertEqual(find_max_score_location(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('Z')), (True, 1, (0, 2)))
    assertEqual(find_max_score_location(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('L')), (True, 0, (0, 1)))
    assertEqual(find_max_score_location(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('T')), (True, 2, (0, 2)))
    assertEqual(find_max_score_location(Grid(3, 5, [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0]]), get_shape('I')), (True, 0, (1, 0)))
    assertEqual(find_max_score_location(Grid(2, 2, []), get_shape('L')), (False, 0, (0, 0)))  # is it a base case that it must fit?
    assertEqual(find_max_score_location(Grid(3, 5, [[1, 0, 0], [0, 0, 0]]), get_shape('L')), (True, 0, (0, 0)))
    assertEqual(find_max_score_location(Grid(3, 4, [[0, 0, 1, 0], [0, 0, 1, 0]]), Shape('', [[True, True, False], [True, True, False]], 'blue')), (True, 0, (0, 0)))
    e = [[False, False, False], [False, False, False], [False, False, False], [False, False, True]]
    #assertEqual(find_max_score_location(Grid(4, 3, e), get_shape('L')), (True, 2, (2, 0)))

    g.print()
