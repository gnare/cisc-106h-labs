from cisc106 import assertEqual
from matplotlib import pyplot
import random
import timeit


def count_singles_state(key, alist):
    """
    Counts the number of occurrences of key in alist. Does not count an occurrence if it is adjacent to itself in the
    list.
    """
    count = 0
    last_two = [None, None]  # this avoids index out of bounds errors and achieves the same thing as alist[i-1] or [i-2]
    for elt in alist:
        if key == elt:
            count += 1
            if last_two[0] == elt:
                count -= 2
                if last_two[1] == elt:
                    count += 1  # fixes a case where odd numbers of consecutive occurrences cause it to subtract 1 extra
        last_two[1] = last_two[0]  # remember the last two items in the list (0 is most recent)
        last_two[0] = elt
    return count


assertEqual(count_singles_state(7, []), 0)
assertEqual(count_singles_state(1, [1, 1, 1]), 0)
assertEqual(count_singles_state(2, [700, 2, 2, 4, 2, 2]), 0)
assertEqual(count_singles_state(9, [9, 7, 9, 9, 0, 5, 9]), 2)
assertEqual(count_singles_state(2, [2, 3, 2, 2, 5, 2, 7, 2, 2, 2, 8, 9, 2]), 3)


def count_singles_index(key, alist):
    """
        Counts the number of occurrences of key in alist. Does not count an occurrence if it is adjacent to itself in the
        list.
        """
    count = 0
    for i in range(len(alist)):
        if key == alist[i]:
            count += 1
            if i > 0 and alist[i - 1] == alist[i]:
                count -= 2
                if i > 1 and alist[i - 2] == alist[i]:
                    count += 1  # fixes a case where odd numbers of consecutive occurrences cause it to subtract 1 extra
    return count


assertEqual(count_singles_index(7, []), 0)
assertEqual(count_singles_index(1, [1, 1, 1]), 0)
assertEqual(count_singles_index(2, [700, 2, 2, 4, 2, 2]), 0)
assertEqual(count_singles_index(9, [9, 7, 9, 9, 0, 5, 9]), 2)
assertEqual(count_singles_index(2, [2, 3, 2, 2, 5, 2, 7, 2, 2, 2, 8, 9, 2]), 3)

alist = list(range(1000))

dependent = []
range_obj = range(100, 1001, 100)
for list_length in range_obj:
    nkey = list_length // 2
    key = random.randint(1, list_length)
    short_list = alist[:list_length]
    short_list[:nkey] = [key] * nkey
    random.shuffle(short_list)
    elapsed = timeit.timeit("count_singles_state(key, short_list)", number=1000, globals=globals())
    dependent += [elapsed]
pyplot.plot(range_obj, dependent, 'r', label="count_singles_state")

dependent = []
for list_length in range_obj:
    nkey = list_length // 2
    key = random.randint(1, list_length)
    short_list = alist[:list_length]
    short_list[:nkey] = [key] * nkey
    random.shuffle(short_list)
    elapsed = timeit.timeit("count_singles_index(key, short_list)", number=1000, globals=globals())
    dependent += [elapsed]
pyplot.plot(range_obj, dependent, 'b', label="count_singles_index")

pyplot.title('Count Singles Function Timings')
pyplot.xlabel('List Size')
pyplot.ylabel('Time (seconds)')
pyplot.legend()
pyplot.savefig('lab07_10.png')
pyplot.show()
