from cisc106 import assertEqual


def look_left(world):
    """
    Returns True if there are any 1s found in list before V is found in world.
    """
    if not world:
        return False
    elif world[0] == '1':
        return True
    elif world[0] == '>' or world[0] == "V" or world[0] == '<':
        return False
    else:
        return look_left(world[1:])


def index_helper(world, idx):
    if not world:
        print("ERROR: Chomper not in world!")
    if world[0] == ">" or world[0] == "V" or world[0] == "<":
        return idx
    else:
        return index_helper(world[1:], idx + 1)


def find_chomper(world):
    """
    Returns the index of chomper in parameter list. May have a helper.
    """
    if not world:
        print("ERROR: Chomper not in world!")
    else:
        return index_helper(world, 0)


def look_right(world):
    """
    Returns true if there are any 1s to right of chomper in world.
    """
    if not world:
        return False
    elif world[-1] == '1':
        return True
    elif world[-1] == '<' or world[-1] == "V" or world[-1] == '>':
        return False
    else:
        return look_right(world[:-1])


def replace(old, new, world):
    """
    Returns world string with first instance of old replaced with new.
        Assumes one space between all other elements.
    Precondition: old is in world.
    """
    if not world:
        return ''
    elif world[0] == old:
        return new + world[1:]
    else:
        return world[0] + replace(old, new, world[1:])


def chomper(world):
    """
    Looks to one direction, if it sees food it
    calls helper with appropriate state. Then checks other
    direction. If no food is found, the program ends.
    """
    print(world)
    chomper_index = find_chomper(world)
    if look_left(world):
        chomp_helper(replace('V', '>', world), '>')
    elif look_right(world):
        chomp_helper(replace('V', '<', world), '<')
    else:
        print('All Done!')


def chomp_helper(world, state):
    """
    Based on chomper state (< or >), looks in one direction for food,
    calls itself on world after one move to left or right. If no food
    is seen in that direction, calls chomper.
    """
    print(world)
    if state == '>':
        if look_left(world):
            chomp_helper(move_one_place_left(world), '>')
        else:
            chomper(replace('>', 'V', world))
    elif state == '<':
        if look_right(world):
            chomp_helper(move_one_place_right(world), '<')
        else:
            chomper(replace('<', 'V', world))


def move_one_place_left(world):
    """ Precondition: there is food to left of chomper. """
    chomper_index = find_chomper(world)
    if chomper_index < 2:
        return world
    elif chomper_index == 2:
        return '> 0' + world[3:]
    else:
        return world[0:chomper_index - 2] + move_one_place_left(world[chomper_index - 2:])


def move_one_place_right(world):
    """ Precondition: there is food to right of chomper. """
    chomper_index = find_chomper(world)
    if world[-1:] == world[chomper_index:]:  # chomper is at the end of the string
        return world
    elif chomper_index == 0:
        return '0 <' + world[3:]
    else:
        return world[0:chomper_index - 1] + ' ' + move_one_place_right(world[chomper_index:])


def test():
    """
    The tests are not sufficient. Identify boundary and other untested
    conditions and write your own tests to augment these.
    """
    assertEqual(move_one_place_left('1 >'), '> 0')
    assertEqual(move_one_place_left('1 0 >'), '1 > 0')
    assertEqual(move_one_place_left('1 > 0'), '> 0 0')
    assertEqual(move_one_place_left('> 1 0'), '> 1 0')

    assertEqual(move_one_place_right('< 1'), '0 <')
    assertEqual(move_one_place_right('< 0 0 0'), '0 < 0 0')
    assertEqual(move_one_place_right('0 0 <'), '0 0 <')

    assertEqual(replace('V', '>', 'V'), '>')
    assertEqual(replace('V', '>', 'V 0 0'), '> 0 0')
    assertEqual(replace('V', '>', '1 1 1 V 0'), '1 1 1 > 0')
    assertEqual(replace('1', '0', '1 1 1'), '0 1 1')

    assertEqual(look_right('V 1'), True)
    assertEqual(look_right('V'), False)
    assertEqual(look_right('V 0 0 1'), True)

    assertEqual(find_chomper('V'), 0)
    assertEqual(find_chomper('>'), 0)
    assertEqual(find_chomper('<'), 0)
    assertEqual(find_chomper('V 0 0 0 0 0 1'), 0)
    assertEqual(find_chomper('0 0 0 0 0 0 0 1 < 0 1 0 1'), 16)
    assertEqual(find_chomper('0 0 V'), 4)

    assertEqual(look_left('1 0'), True)
    assertEqual(look_left('0 1'), True)
    assertEqual(look_left('V'), False)
    assertEqual(look_left('0 0 0 0 0 V'), False)
    assertEqual(look_left('> 0 0 0 0 0 1'), False)

    chomper('0 0 0 V 0 0 0')
    chomper('1 0 0 V 0 0 0')
    chomper('0 0 1 V 1 0 0 0')
    chomper('1 1 V 1 1')


test()
