# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def distance(acceleration, velocity, time):
    return 0.5 * acceleration * time ** 2 + velocity * time  # calculates distance traveled given the acceleration, velocity, and time


assertEqual(distance(1, 1, 1), 1.5)  # check that the function correctly calculates distance
assertEqual(distance(0, 5, 10), 50)
assertEqual(distance(10, 0, 4), 80)
