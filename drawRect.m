function newPic = drawRect(pic, startX, startY, endX, endY, layer, newValue)   
    newPic = pic;
    newPic(startY:endY, startX:endX, layer) = newValue;
end