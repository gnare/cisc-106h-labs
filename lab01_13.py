# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def getIthDigit(n, i):
    return (abs(n) // 10 ** (
                i - 1)) % 10  # find the digit in the i-th place by dividing by a power of 10 and then taking 10 mod of the result


assertEqual(getIthDigit(1234567, 5), 3)  # test positives
assertEqual(getIthDigit(23948234, 3), 2)
assertEqual(getIthDigit(-481902374, 4), 2)  # test negatives
assertEqual(getIthDigit(-5938475, 6), 9)
assertEqual(getIthDigit(-999976423453498693487534587121234, 16), 3)  # test exorbitantly large numbers
assertEqual(getIthDigit(923784826734876826348276348786478, 18), 8)
