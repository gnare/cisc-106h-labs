# -*- coding: utf-8 -*-
# Derik Spedden, Galen Nare, and Will Quintana
from cisc106 import assertEqual


def gridMatch(alist, blist):
    """
    Returns true if any two 2D nested lists of the same size and shape have an ’x’ in all the same places.
    """
    result = None
    for elt1 in range(len(alist)):
        for elt2 in range(len(alist[elt1])):
            if alist[elt1][elt2] == 'x' and blist[elt1][elt2] == 'x':
                result = True
            elif alist[elt1][elt2] != 'x' and blist[elt1][elt2] != 'x':
                result = True
            else:
                return False
    return result


assertEqual(gridMatch([[1, 2], ['y', 'z']], [[9, 'wombat'], [7, 'spam']]), True)
assertEqual(gridMatch([[1, 'x'], ['x', 'z']], [[9, 'x'], ['x', 'spam']]), True)
assertEqual(gridMatch([[1, 'x'], ['x', 'z']], [[9, 'x'], ['x', 'x']]), False)
