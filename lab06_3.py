def get_first_odd():
    """
    Takes a number as input and returns it if it is odd. Will continue to take input until an odd number is given.
    """
    entry = int(input("Please enter an odd number: "))
    while entry % 2 == 0:
        entry = int(input("That was an even number. Please enter an odd number: "))
    return entry
