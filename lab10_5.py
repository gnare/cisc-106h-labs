# Derik Spedden, Galen Nare, and Will Quintana
from cisc106 import assertEqual


def rotate90(amatrix):
    """
    Rotates a 2-dimensional list (amatrix) 90 degrees clockwise.
    The rows become the columns and the columns become the rows.
    """
    if not amatrix:
        return amatrix
    new_matrix = [list(a) for a in zip(*amatrix)]
    return new_matrix


assertEqual(rotate90([]), [])
assertEqual(rotate90([[1, 2], [3, 4]]), [[1, 3], [2, 4]])
assertEqual(rotate90([[2, 4, 3], [5, 8, 1]]), [[2, 5], [4, 8], [3, 1]])

assertEqual(rotate90([[True, False], [True, False], [True, True]]), [[True, True, True], [False, False, True]])
assertEqual(rotate90([[True, True, True], [True, False, True]]),
            [[True, True], [True, False], [True, True]])
assertEqual(rotate90([[False, True], [True, True], [False, True]]),
            [[False, True, False], [True, True, True]])
assertEqual(rotate90([[False, True, False], [True, True, True]]),
            [[False, True], [True, True], [False, True]])
assertEqual(rotate90([]), [])
assertEqual(rotate90([[1, 2], [3, 4]]), [[1, 3], [2, 4]])
assertEqual(rotate90([[2, -4, 3], [5, 8, 1]]), [[2, 5], [-4, 8], [3, 1]])
assertEqual(rotate90([[1, 2, 3]]), [[1], [2], [3]])
