from cisc106 import assertEqual


def replace_all(to_replace, replacement, par1_list):
    """
    Replaces all occurrences of to_replace with replacement in par1_list
    """
    if not par1_list:
        return []
    else:
        if par1_list[0] == to_replace:
            return [replacement] + replace_all(to_replace, replacement, par1_list[1:])
        else:
            return [par1_list[0]] + replace_all(to_replace, replacement, par1_list[1:])


assertEqual(replace_all(5, 7, []), [])
assertEqual(replace_all('yes', 'no', ['yes', 'yes', 'yes', 'yes']), ['no', 'no', 'no', 'no'])
assertEqual(replace_all(2, 3, [1, 2, 3, 4, 5]), [1, 3, 3, 4, 5])
