# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def close_enough(a, b, distance):
    '''
        Return True if the difference between a and b is less than the threshold (distance)
        Otherwise return False.
    '''
    return abs(a - b) <= distance


assertEqual(close_enough(3, 7, 4), True)  # check that 3 and 7 are correctly processed as no more than 4 apart
assertEqual(close_enough(0, 100, 1), False)  # check that 0 and 100 are correctly processed as being more than one apart
assertEqual(close_enough(50, 40, 12), True)  # check that 50 and 40 are correctly processed as no more than 12 apart
