from cisc106 import assertEqual


def countUniqueKeys(alist):
    """
    Counts unique elemnts in a list or other iterable and returns the number of times each element occurs in the iterable,
    as a dictionary.
    """
    key_counts = {}
    for elt in alist:
        if elt not in key_counts:
            key_counts[elt] = 1
        else:
            key_counts[elt] += 1
    return key_counts


assertEqual(countUniqueKeys([]), {})
assertEqual(countUniqueKeys([1, 2, 3]), {1: 1, 2: 1, 3: 1})
assertEqual(countUniqueKeys(['pie', 'cake', 'pie', 'orange', 'donut', 'donut']), {'pie': 2, 'cake': 1, 'orange': 1, 'donut': 2})
assertEqual(countUniqueKeys("Hello world!"), {'H': 1, 'e': 1, 'l': 3, 'o': 2, ' ': 1, 'w': 1, 'r': 1, 'd': 1, '!': 1})
