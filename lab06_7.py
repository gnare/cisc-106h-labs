from cisc106 import assertEqual


def contains(key, alist):
    """
    Returns True if key is in alist
    """
    for elt in alist:
        if elt == key:
            return True
    return False


assertEqual(contains(0, []), False)
assertEqual(contains(7, [2, 4, 7, 9, 4]), True)
assertEqual(contains('apple', ['apple', 'banana', 'orange']), True)
assertEqual(contains('pi', [3.14]), False)
assertEqual(contains('b', 'wombat'), True)
assertEqual(contains('n', 'microwave'), False)
