import random


def guess_my_number():
    """
    Guessing game that chooses a random number and gives feedback on guesses
    """
    num_guesses = 1
    guess = int(input("your guess: "))
    n = random.Random().randint(0, 100)
    while guess != n:
        if n > guess:
            guess = int(input("Too low. Your next guess: "))
        else:
            guess = int(input("Too high. Your next guess: "))
        num_guesses += 1
    print("Correct! That took " + str(num_guesses) + " guesses!")
